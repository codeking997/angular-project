import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TrainerPageComponent } from './components/shared/trainer-page/trainer-page.component';
import { PokemonCatalogueComponent } from './components/shared/pokemon-catalogue/pokemon-catalogue.component';
import { PokemonDetailComponent } from './components/shared/pokemon-detail/pokemon-detail.component';
import { LandingPageComponent } from './components/shared/landing-page/landing-page.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from './data.service';

@NgModule({
  declarations: [
    AppComponent,
    TrainerPageComponent,
    PokemonCatalogueComponent,
    PokemonDetailComponent,
    LandingPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    HttpClientModule 
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
