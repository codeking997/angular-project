import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from '../pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {
  //sets the name from the values in local storage, also sets the pokemon array to the value in local storage
  constructor(private router:Router) { 
    this.trainer.name = localStorage.getItem('name');
    this.trainer.pokemonArray = JSON.parse(localStorage.getItem('pokemonArray'))??[];
  }

  trainer = {
    name: '',
    pokemonArray: []
  }
  //checks the local storage and initialises name to equal null
  checkStorage(){
    return localStorage.getItem('name')===null;
  }
  //sets the name and parses it as json
  setName(){
    localStorage.setItem('name', JSON.stringify(this.trainer.name));
  }
  getName(){
   
    return  this.trainer.name;
  
  }
  //adds a pokemon, it pushes a pokemon to the pokemon array and then resets it in local storage
  addPokemon(pokemon){
    
    this.trainer.pokemonArray.push(pokemon);
    localStorage.setItem('pokemonArray', JSON.stringify(this.trainer.pokemonArray));
    console.log(pokemon);
  }
  getTrainer(){
    return this.trainer;
  }

}
