import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pokemon } from './pokemon.model';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  

  constructor(private http: HttpClient) { }

  getAllPokemon():Promise<any>{
    
    return this.http.get('https://pokeapi.co/api/v2/pokemon').toPromise();
  }
  getById(id):Promise<any>{
    
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/${id}`).toPromise();
  }
  
}
