import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Pokemon } from '../../../pokemon.model';
import { DataService } from '../../../data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.css']
})
export class PokemonCatalogueComponent implements OnInit {


  constructor(private data:DataService, private router: Router, private route: ActivatedRoute) { }
  public pokemonList=[];
  @Output() clickDetail :  EventEmitter<number> = new EventEmitter();

 //checks if there is a name on the landing page, if null then it goes to the landing page
  ngOnInit(): void {
    if(localStorage.getItem('name')===null){
      this.router.navigateByUrl('/landing-page');
    }
    this.getAllPokemon();

    this.route.snapshot.paramMap.get('id');

    
  }
  
  /*
  the method takes in a parameter of id which is the same id that can be found in the pokemon url
  it will then navigate to pokemon detail and add on that specific id to the url
  making it possible to redirect and see a detail page of a pokemon
  */
  onClickedDetail(id){
    this.router.navigate(['/pokemon-detail/', id]);
    console.log("testing");
  }

  /*
  this method takes in data from the data service
  it then loops through all the pokemons using a foreach loop
  the loop creates new properties in the pokemon object like pokemon url.
  const som splits up the url of each pokemon and stores the id that each pokemon has
  it then creates a new pokemon object property of id. 
  the pokemon photo object property takes in the photo urls and appends the id to it, 
  this will then return a photo for each of the pokemons.
  */
  async getAllPokemon (){
    try {

      const x = await this.data.getAllPokemon();
      
      this.pokemonList = x.results;
      this.pokemonList.forEach((pokemon)=>{
        const url = pokemon.url
        console.log(pokemon);
        const som =  url.split('/').filter(Boolean).pop()
        console.log(som)
        pokemon.id = som;
        pokemon.photo =   `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${som}.png`
        
        
        
    });
    
      

    } catch (error) {
        console.error(error);
    }
  }
  
  
  

}
