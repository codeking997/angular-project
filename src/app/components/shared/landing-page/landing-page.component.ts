import { JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor(private session: SessionService, private router: Router) { }

  

  //a trainer object that takes in the name of the trainer an has an array that takes in pokemon objects
  trainer = {
    name: '',
    pokemonArray: []
  }
  

 /*
 the if checks if there exists a name in local storage, if there is not it sets the trainer name to an empty string
if there is a name it will automatically redirect the user to the training page
 */
  getName(){
    if(localStorage.getItem('name')===null){
      this.trainer.name = '';
    }else {
      this.trainer.name = JSON.parse(localStorage.getItem('name'));
      //go to trainerpage
      this.router.navigateByUrl('/trainer-page');
    }
  }
  //this method stores the input text in local storage and then redirects to the trainer page
  getText(){
    localStorage.setItem('name', JSON.stringify(this.trainer.name));
    this.router.navigateByUrl('/trainer-page');
  }

  ngOnInit(): void {
    this.getName();
    
    //console.log(this.coaches);
  }

}
