import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { ActivatedRoute } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { TrainerService } from 'src/app/trainer/trainer.service';



@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {

  constructor(private router:Router, private data:DataService, private route: ActivatedRoute, private http: HttpClient, private pokeServ:TrainerService) { }
  public pokemonDetailList=[];
  @Output() clickDetail :  EventEmitter<number> = new EventEmitter();
  public pokemon = {
          name: '',
          idURL: '',
          id: '',
          photo: '',
          url: '', 
          types: [],
          baseStat: [], 
          height: Number,
          weight: Number,
          abilities: [],
          baseExperience: Number, 
          moves: []
  }; 
 //checks if there is a name in local storage, if there is no name it will go to the landing page.
  async ngOnInit(){
    if(localStorage.getItem('name')===null){
      this.router.navigateByUrl('/landing-page');
    }
    
    
    const yes = this.route.snapshot.paramMap.get('id');
    this.pokemon = await this.getById(yes);
  }
  //will go to the pokemon detail page
  onClickedDetail(){
    this.router.navigate(['/pokemon-detail', ]);
  }
  //will catch a pokemon
  catchPokemon(){

    console.log("the button is working");
    this.pokeServ.addPokemon(this.pokemon);
  }
 /*
 this method is a little bit too long but this method gets the pokemon by its id, 
 it then has several for loops where it goes through to print out the stats of each pokemon
 from the api
 */
  async getById(id){
    try {
        let pokemonElement = {
          name: '',
          idURL: '',
          id: '',
          photo: '',
          url: '', 
          types: [],
          baseStat: [], 
          height: Number,
          weight: Number,
          abilities: [],
          baseExperience: Number, 
          moves: []
        }
        const url = pokemonElement.url; 
        
        console.log(id);
        pokemonElement.idURL = `https://pokeapi.co/api/v2/pokemon/${id}/`
        pokemonElement.id = id;
        pokemonElement.photo =   `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`
        
        const ApiResult = await this.http.get(`https://pokeapi.co/api/v2/pokemon/${id}/`).toPromise();
        pokemonElement.name = ApiResult['name'];
        //will loop through and find the types of the pokemon and push it to the types array
        for (const type of ApiResult['types']) {
          pokemonElement.types.push(type.type.name);
        }
        //will go through the api and push the baseStat to the base stat array
        for (const type of ApiResult['stats']) {
          pokemonElement.baseStat.push(type.base_stat);
        }
        console.log( pokemonElement.baseStat, 'basestats');
        //sets the height and the weight to the result of the api weight and height
        pokemonElement.height = ApiResult['height'];  
        pokemonElement.weight = ApiResult['weight'];
        console.log(pokemonElement.weight);
        console.log(pokemonElement.height);
        //loops through the abilites and pushes the abilities to the abilities array
        for (const type of ApiResult['abilities']) {
          pokemonElement.abilities.push(type.ability.name);
        }
        console.log(pokemonElement.abilities);
       
        pokemonElement.baseExperience = ApiResult['base_experience'];
        //loops through and pushes it the moves array  
        for (const type of ApiResult['moves']) {
          pokemonElement.moves.push(type.move.name);
        }
        console.log(pokemonElement.moves);
        console.log(ApiResult, 'api');
        
        console.log(pokemonElement);
        
        return pokemonElement;



      
    } catch (error) {
        console.error(error);
    }
  }
}
