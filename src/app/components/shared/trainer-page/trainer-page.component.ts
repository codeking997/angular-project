import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TrainerService } from 'src/app/trainer/trainer.service';


@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css']
})
export class TrainerPageComponent implements OnInit {

  constructor(private router: Router, private pokeServ:TrainerService, private route: ActivatedRoute) { }
  /*
  checks if there is a name in local storage and if there is not it will redirect you to the landing page
  will call pokeServ from the data service and get the trainer from there
  takes in an id 
  */
  ngOnInit(): void {
    if(localStorage.getItem('name')===null){
      this.router.navigateByUrl('/landing-page');
    }
     this.trainer = this.pokeServ.getTrainer();
     this.route.snapshot.paramMap.get('id');
  }
  trainer = {
    name: '',
    pokemonArray: []
  }
  //takes the user to the pokemon detail page when clicking on a pokemon
  onClickedDetail(id){
    this.router.navigate(['/pokemon-detail/', id]);
    console.log("testing");
  }
}

