import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './components/shared/landing-page/landing-page.component';
import { PokemonCatalogueComponent } from './components/shared/pokemon-catalogue/pokemon-catalogue.component';
import { PokemonDetailComponent } from './components/shared/pokemon-detail/pokemon-detail.component';
import { TrainerPageComponent } from './components/shared/trainer-page/trainer-page.component';

const routes: Routes = [
  {
    path: 'landing-page',
    
    component: LandingPageComponent
  },
  {
    path: 'trainer-page',
    component: TrainerPageComponent
  }, 
  {
    path: 'pokemon-detail/:id',
    component: PokemonDetailComponent
  },
  {
    path: 'pokemon-catalogue', 
    component: PokemonCatalogueComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landing-page'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
